from datetime import datetime
from stix2 import Malware, Indicator, Relationship, Bundle
from json import dumps
import uuid


def create_stix(item):
    DATETIME = "%s" % (datetime.now())
    try:
        malware_list = ['Surplus Stats']

        for key, value in item.iteritems():
            if key != 'tags':
                try:
                    stat = '%s = [%s]' % (key, value)
                    malware_list.append(stat)
                except:
                    malware_list.append('An error occured with '+key)

        # Create Malware object
        try:
            malware = Malware(
                id="malware--"+item['sid'],
                created=DATETIME,
                modified=DATETIME,
                name=item['name'],
                labels=malware_list#, score, magic, md5, sha1, sha256, size, seen_first, seen_last]
            )
        except:
            malware = Malware(
                id="malware--aaa00a00-a00a-00a0-a0a0-a00aaa00a000",
                created="2014-02-20T09:16:08.989Z",
                modified="2014-02-20T09:16:08.989Z",
                name=item['name'],
                labels=["Surplus Stats"]
            )

        # Create Indicator object
        try:
            indicator = Indicator(
                id="indicator--"+item['sid'],
                created=DATETIME,
                modified=DATETIME,
                name="TAGS",
                description="The list of applicable tags for "+item['name']+".",
                labels=item['tags'],
                pattern="[file:hashes.'SHA-256' = '"+item['sha256']+"']"
        )
        except:
            indicator = Indicator(
                id="indicator--aaa00a00-a00a-00a0-a0a0-a00aaa00a000",
                created="2014-02-20T09:16:08.989Z",
                modified="2014-02-20T09:16:08.989Z",
                name="TAGS[error]",
                description="There has been an issue creating the indicator object.",
                labels=["malicious-activity"],
                pattern="[file:hashes.'SHA-256' = '0000000000000000000000000000000000000000000000000000000000000000']"
            )

        relationship = Relationship(indicator, 'attributed-to', malware)

        bundle = Bundle(objects=[indicator, malware, relationship])
        output = '%s' %bundle
        return output
    except:
        errMsg = 'An error has occurred while creating the STIX object\n' + error_data()
        return errMsg


def get_tags(data):
    try:
        tags = []
        for key, value in data.iteritems():
            if key == 'tags':
                if len(value) > 0:
                    tags.extend(value)
            elif isinstance(value, dict):
                newValue = get_tags(value)
                if len(newValue) > 0:
                    tags.extend(newValue)

        return tags
    except:
        return ['An error has occurred with the tags']

def get_score(data):
    try:
        score = 0
        for key, value in data.iteritems():
            if key == 'results':
                for dict in value:
                    for k, v in dict.iteritems():
                        if k == 'result':
                            score += v.get('score')
        return score
    except:
        return None



def stixDump(data, children=[]):
    full_data = data
    global full_data
    try:
        temp = uuid.uuid1()
        sid = "%s" % (temp)
    except:
        sid = "aaa00a00-a00a-00a0-a0a0-a00aaa00a000"
    try:
        info = data['file_info']
        file = {}
        file['name'] = info.get('path')
        file['magic'] = info.get('magic')
        file['sid'] = sid
        file['md5'] = info.get('md5')
        file['sha1'] = info.get('sha1')
        file['sha256'] = info.get('sha256')
        file['seen_count'] = info.get('seen_count')
        file['seen_first'] = info.get('seen_first')
        file['seen_last'] = info.get('seen_last')
        file['size'] = info.get('size')
        file['score'] = get_score(data)
        file['children'] = children
        tags = get_tags(data)
        file['tags'] = tags
        if len(tags) == 0:
            file['tags'] = ['Empty']

        for key, value in file.iteritems():
            try:
                if value == None:
                    file[key] = '[Data missing]'
            except:
                file[key] = 0

        return create_stix(file)

    except:
        errMsg = 'An error has occurred\n' + error_data()
        return errMsg


def error_data():
    return dumps(full_data, sort_keys=True, indent=4, separators=(',', ': '))


def get_children(data):
    srls = []
    try:
        children = data.get('childrens')
        for child in children:
            srls.append(child.get('srl'))
        return srls
    except:
        return []
